//
//  ViewController.swift
//  GOT
//
//  Created by Christian Varriale on 25/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController {
    
    //MARK: - Properties
    var descriptionCharacter = [
        "Daenerys":"Daenerys Targaryen is the daughter of King Aerys II Targaryen (also referred to as The Mad King) and his sister-wife Queen Rhaella, and is one of the last survivors of House Targaryen.",
        "Jon Snow":"Jon Snow is introduced as the 14-year-old illegitimate son of Eddard Ned Stark, Lord of Winterfell, and half-brother to Robb, Sansa, Arya, Bran and Rickon",
        "Joffrey Baratheon":"In public, Joffrey is allegedly the oldest son and heir of King Robert Baratheon and Queen Cersei Lannister, both of whom entered into a political marriage alliance after Robert took the throne by force from the Mad King Aerys II Targaryen.",
        "Sansa Stark":"Sansa Stark is the second child and elder daughter of Eddard Stark and Catelyn Stark. She was born and raised in Winterfell, until leaving with her father and sister at the beginning of the series",
        "Nothing":"No Description"
    ]
    
    //MARK: - IBOutlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var button: UIButton!
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.20232445, green: 0.27087152, blue: 0.3563757539, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))]
        
        view.backgroundColor = #colorLiteral(red: 0.8343606591, green: 0.8158759475, blue: 0.767637074, alpha: 1)
        
        descriptionView.layer.backgroundColor = #colorLiteral(red: 0.8343606591, green: 0.8158759475, blue: 0.767637074, alpha: 1)
        button.backgroundColor = #colorLiteral(red: 0.3950665295, green: 0.4004094303, blue: 0.3870662451, alpha: 1)
        button.layer.borderWidth = 1
        button.layer.borderColor = #colorLiteral(red: 0.3950665295, green: 0.4004094303, blue: 0.3870662451, alpha: 1)
        button.tintColor = .white
        button.layer.cornerRadius = button.frame.height/2
    }
    
    //MARK: - IBAction
    @IBAction func pickImageButtonPressed(_ sender: Any) {
        
        //Defining the Tipology of alert, that we want to present to the users
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //Defining Camera Action
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
            DispatchQueue.main.async {
                self.presentImagePicker(withType: .camera)
            }
        }
        
        //Defining Gallery Action
        let libraryAction = UIAlertAction(title: "Photo Library", style: .default) { _ in
            DispatchQueue.main.async {
                self.presentImagePicker(withType: .photoLibrary)
            }
        }
        
        //Defining Cancel Action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        //Add Action to the Controller
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        actionSheet.addAction(cancelAction)
        
        //Show View
        present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: Private Methods
    private func presentImagePicker(withType type: UIImagePickerController.SourceType) {
        //Definiton of imagePickerController with Delegate, sourceType and Present
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = type
        present(pickerController, animated: true)
    }
    
    //MARK: - Function to Detect Character
    func detect(image: CIImage){
        
        //Load Model in the guard let
        guard let model = try? VNCoreMLModel(for: GoTActorClassifier94().model) else { fatalError("Loading CoreML Model Failed.") }
        
        //Create request from the model loaded
        let request = VNCoreMLRequest(model: model) { (request, error) in
            
            //From request take an array of Any that i will cast to VNClassificationObservation
            guard let observation = request.results else { fatalError("Error about the request.") }
            
            //Classifications will be an array of string (Possible label to show): compact delete the nil value, filter result for confidence > 90% and with map take only the the string
            let classifications = observation
                .compactMap({$0 as? VNClassificationObservation})
                .filter({$0.confidence > 0.9})
                .map({$0.identifier})
            
            //Change the label in the main thread
            DispatchQueue.main.async {
                switch classifications.first {
                case "Jon Snow":
                    print(classifications.first as Any)
                    self.label.text = "Jon Snow"
                    self.descriptionView.text = self.descriptionCharacter["Jon Snow"]
                case "Daenerys":
                    print(classifications.first as Any)
                    self.label.text = "Daenerys"
                    self.descriptionView.text = self.descriptionCharacter["Daenerys"]
                case "Joffrey Baratheon":
                    print(classifications.first as Any)
                    self.label.text = "Joffrey Baratheon"
                    self.descriptionView.text = self.descriptionCharacter["Joffrey Baratheon"]
                case "Sansa Stark":
                    print(classifications.first as Any)
                    self.label.text = "Sansa Stark"
                    self.descriptionView.text = self.descriptionCharacter["Sansa Stark"]
                default:
                    print(classifications.first as Any)
                    self.label.text = "Nothing"
                    self.descriptionView.text = self.descriptionCharacter["Nothing"]
                }
            }
            
        }
        
        //Handler for request of image classification
        let handler = VNImageRequestHandler(ciImage: image)
        
        //Perform the handler have the highest priority
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                
                try handler.perform([request])
                
            } catch {
                print(error)
            }
        }
    }
    
}

// MARK: - UINavigationControllerDelegate + UIImagePickerControllerDelegate
extension ViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    //Upload image in the picker and convert it
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        dismiss(animated: true)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("Couldn't load image picked image")
        }
        
        imageView.image = image
        
        guard let ciImage = CIImage(image: image) else {
            fatalError("Couldn't convert UIImage to CIImage")
        }
        
        detect(image: ciImage)
    }
    
}
